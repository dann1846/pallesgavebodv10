﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PallesGavebodV10.Data;
using PallesGavebodV10.Models;

namespace PallesGavebodV10.Controllers
{
	public class GiftController : Controller
	{
		private readonly GiftDbContext _context;

		public GiftController(GiftDbContext context)
		{
			_context = context;
		}

		[HttpGet]
		public IActionResult GirlGifts()
		{
			return View(_context.Gifts.ToList().Where(x => x.GirlGift && x.BoyGift == false));
		}

		// GET: Gift
		[HttpGet]
		public async Task<IActionResult> Index()
		{
			return View(await _context.Gifts.ToListAsync());
		}

		// GET: Gift
		[HttpGet("/Gift/FromDate/{year}/{month}/{day}/ToDate/{year2}/{month2}/{day2}")]
		public async Task<IActionResult> Index(int year, int month, int day, int year2, int month2, int day2)
		{
			DateTime dateFrom = new DateTime(year, month, day);
			DateTime dateTo = new DateTime(year2, month2, day2);
			return View(await _context.Gifts.Where(x => x.CreationDate >= dateFrom && x.CreationDate <= dateTo).ToListAsync());
		}

		// GET: Gift/Details/5
		[HttpGet]
		public async Task<IActionResult> Details(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var gift = await _context.Gifts
				 .FirstOrDefaultAsync(m => m.GiftNumber == id);
			if (gift == null)
			{
				return NotFound();
			}

			return View(gift);
		}

		// GET: Gift/Create
		[Authorize(Roles = "Admin")]
		[HttpGet]
		public IActionResult Create()
		{
			return View();
		}

		// POST: Gift/Create
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("GiftNumber,Title,Description,BoyGift,GirlGift")] Gift gift)
		{
			if (ModelState.IsValid)
			{
				gift.CreationDate = DateTime.Now;
				_context.Add(gift);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(gift);
		}

		// GET: Gift/Edit/5
		[HttpGet]
		public async Task<IActionResult> Edit(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var gift = await _context.Gifts.FindAsync(id);
			if (gift == null)
			{
				return NotFound();
			}
			return View(gift);
		}

		// POST: Gift/Edit/5
		// To protect from overposting attacks, enable the specific properties you want to bind to, for 
		// more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Edit(int id, [Bind("GiftNumber,Title,Description,CreationDate,BoyGift,GirlGift")] Gift gift)
		{
			if (id != gift.GiftNumber)
			{
				return NotFound();
			}

			if (ModelState.IsValid)
			{
				try
				{
					_context.Update(gift);
					await _context.SaveChangesAsync();
				}
				catch (DbUpdateConcurrencyException)
				{
					if (!GiftExists(gift.GiftNumber))
					{
						return NotFound();
					}
					else
					{
						return StatusCode(409);
					}
				}
				return RedirectToAction(nameof(Index));
			}
			return View(gift);
		}

		// GET: Gift/Delete/5
		[HttpGet]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id == null)
			{
				return NotFound();
			}

			var gift = await _context.Gifts
				 .FirstOrDefaultAsync(m => m.GiftNumber == id);
			if (gift == null)
			{
				return NotFound();
			}

			return View(gift);
		}

		// POST: Gift/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> DeleteConfirmed(int id)
		{
			var gift = await _context.Gifts.FindAsync(id);
			_context.Gifts.Remove(gift);
			await _context.SaveChangesAsync();
			return RedirectToAction(nameof(Index));
		}

		private bool GiftExists(int id)
		{
			return _context.Gifts.Any(e => e.GiftNumber == id);
		}
	}
}
