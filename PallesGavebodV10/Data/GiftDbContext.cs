﻿using Microsoft.EntityFrameworkCore;
using PallesGavebodV10.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PallesGavebodV10.Data
{
	public class GiftDbContext : DbContext
	{
		public DbSet<Gift> Gifts { get; set; }

		public GiftDbContext(DbContextOptions<GiftDbContext> options) : base(options)
		{
			//Database.EnsureCreated();
		}
	}
}